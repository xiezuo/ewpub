-- --------------------------------------------------------
--
-- 测试数据库，包含测试用户表与作品表，仅供参考
--

-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-09-05 14:24:06
-- 服务器版本： 5.6.27-log
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `ewpub`
--
CREATE DATABASE IF NOT EXISTS `ewpub` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ewpub`;

-- --------------------------------------------------------

--
-- 表的结构 `book`
--

CREATE TABLE `book` (
  `id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `summary` mediumtext,
  `tag` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `created` bigint(20) UNSIGNED DEFAULT NULL,
  `updated` bigint(20) UNSIGNED DEFAULT NULL,
  `uid` int(10) NOT NULL DEFAULT '0',
  `filepath` varchar(255) DEFAULT NULL,
  `bid` varchar(50) NOT NULL DEFAULT '0',
  `showall` int(10) NOT NULL DEFAULT '-1',
  `updateDes` varchar(255) DEFAULT NULL,
  `bookcover` varchar(255) DEFAULT NULL,
  `booksize` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT 'user8888',
  `sex` varchar(50) DEFAULT '',
  `province` varchar(255) DEFAULT '',
  `city` varchar(255) DEFAULT '',
  `created` bigint(20) DEFAULT NULL,
  `updated` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `description` text,
  `isactive` tinyint(3) DEFAULT '1',
  `age` int(10) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT '',
  `client_app` varchar(255) DEFAULT NULL,
  `client_uid` varchar(255) DEFAULT NULL,
  `accesstoken` varchar(255) DEFAULT NULL,
  `usertype` varchar(255) DEFAULT NULL,
  `client_device` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bid` (`bid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `book`
--
ALTER TABLE `book`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

 -- --------------------------------------------------------

--
-- 表的结构 `chapter`
--

CREATE TABLE `chapter` (
  `id` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `uid` int(10) NOT NULL DEFAULT '0',
  `bid` varchar(50) NOT NULL DEFAULT '0',
  `cid` varchar(50) NOT NULL DEFAULT '0',
  `updated` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapter`
--
ALTER TABLE `chapter`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `chapter`
--
ALTER TABLE `chapter`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;COMMIT;
