<?php
/**
 * 验证用户请求的合法性
 * appid 传入的APPID，需与本地一一对应
 * timestamp 传入的时间戳
 * signture 传入的加密签名，通过MD5计算后与本地的加密签名进行验证
 */
class BaseAuth implements iAuthenticate
{
    public function __isAuthenticated()
    { //验证appid 签名
        if (!empty($_GET['appid']) && !empty($_GET['timestamp']) && !empty($_GET['signture'])) {
            if (APP_ID != $_GET['appid']) { //传入的appid与系统中的不一致
                throw new RestException(401, '不正确的APPID!');
            }
            //根据appid和appsecret和时间戳加密的签名验证有效性
            $signture = md5($_GET['appid'] . APP_SECREAT . $_GET['timestamp']);
            if ($signture == $_GET['signture']) { //签名验证正确
                return true;
            } else {
                throw new RestException(401, '签名错误!' . $signture);
            }
        }
        throw new RestException(401, '参数传递错误!');
    }

}
