<?php

/* 此部分处理壹写作发送的用户验证数据，基本流程如下
 * 用户未被授权，请根据传入的资料注册新的用户并返回
 * 用户已授权，从您的数据库中查找用户并返回数据内容
 */
class User
{
    static $table = 'user';
    static $table_short = 'u';
    public function __construct()
    {
        $this->db = Mysql::getInstance();

    }
    protected function get($request_data = null)
    {
        $params = json_decode(json_encode($request_data), true);
        return $params;
    }
    /**
     * 用户自动注册或登录
     * client_app:app名称
     * client_uid:用户id
     * openid:已授权用户分配的openid
     * accesstoken:已授权用户分配的accesstoken
     * 我们发送的内容包括
     * params={
     *
     * }
     */
    protected function post($request_data = null)
    {
        $params = json_decode(json_encode($request_data), true);
        //身份验证相关参数已在baseauth使用，不再需要
        unset($params['signture']);
        unset($params['timestamp']);
        unset($params['appid']);
        // TODO:此部分逻辑请根据您的库进行处理，以下为示例
        // 示例中将本地数据库的用户标识 id 传递至壹写作 为 openid

        $ret = array();
        $foundUser = null;
        //使用用户名密码登录的老用户
        if ($params['usertype'] == "third" || $params['usertype'] == "bind") {
            $foundUser = $this->db->rs('*', 'user', array("name" => $params['name'], "password" => $params['password']));
            if (!$foundUser['total']) {
                $ret["code"] = 3; //找不到用户,返回错误
                return $ret;
            }
        } else {
            //已经绑定的用户
            if ($params['openid'] && $params['accesstoken']) {
                $foundUser = $this->db->rs('*', 'user', array("client_app" => $params['client_app'], "client_uid" => $params['client_uid'], "id" => $params['openid'], "accesstoken" => $params['accesstoken'])); //查询符合条件的用户
            }
        }
        //身份验证相关参数不再需要
        unset($params['openid']);
        unset($params['accesstoken']);
        // 找到用户资料，直接返回用户资料
        if ($foundUser['total']) {
            if ($params['usertype'] == "third"){
                $user = $foundUser["data"][0];
                $ret["code"] = 1;
                $ret["accesstoken"] = $user["accesstoken"];
                $ret["openid"] = $user["id"];
                $ret['user'] = $user;
                $ret["username"] = $user["name"];
                return $ret;
            }
            elseif ($params['usertype'] == "bind"){
                $user = $foundUser["data"][0];
                $ret["state"] = 'thirdLogin';
                $ret["appid"] = APP_ID;
                $ret["code"] = 1;
                $ret["accesstoken"] = $user["accesstoken"];
                $ret["openid"] = $user["id"];
                $ret['timestamp'] = time();
                $ret["username"] = $user["name"];
                $url = 'http://appchina.1xiezuo.com/thirdapi/logincallback.php?'. http_build_query($ret);
                header('Location: '.$url); 
            }
           
        } else {
            $password = $this->generate_password();
            $params["password"] = $password; //初始密码
            $params["accesstoken"] = md5($password); //加密
            //未找到用户资料，新建关联用户
            $newUser = $this->db->insert(self::$table, $params);
            if ($newUser['result'] == true) {
                // 为了用户下次能够通过您的网站进行登录完善资料，可在此处将用户初始密码一并发送
                $data = $this->db->rs('*', 'user', array("id" => $newUser['id']));
                $user = $data["data"][0];
                $ret["code"] = 2;
                $ret["accesstoken"] = $user["accesstoken"];
                $ret["openid"] = $user["id"];
                $ret['user'] = $user;
                //明文返回用户名密码以便用户登录或通过邮件将用户名密码发送至用户邮箱
                $ret["username"] = $user["name"];
                $ret["password"] = $user["password"];
                $ret["sendmail"] = true; //如果发送了邮件，请使用true
                return $ret;
            } else {
                return $newUser;
            }
        }
    }
    /**
     * 解除用户绑定
     * client_app:app名称
     * client_uid:用户id
     * openid:已授权用户分配的openid
     * accesstoken:已授权用户分配的accesstoken
     */
    protected function delete($id = null, $request_data = null)
    {
        $params = json_decode(json_encode($request_data), true);
        unset($params['signture']);
        unset($params['timestamp']);
        unset($params['appid']);
        // TODO:此部分逻辑请根据您的库进行处理，以下为示例

        $return = $this->db->destroy(self::$table, $params['openid']);
        if ($return['result'] == true) {
            $ret['code'] = 2;
            $ret['msg'] = "用户解除绑定成功";
            return $ret;
        } else {
            return $return;
        }
    }
    private function generate_password($length = 8)
    {
        // 密码字符集，可任意添加你需要的字符
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $password = '';
        for ($i = 0; $i < $length; $i++) {
            // $password .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
            //  mt_rand: 使用 Mersenne Twister 算法返回随机整数
            // strlen:返回字符串的长度
            $password .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $password; //返回加密后的密码
    }
}
