<?php

//TODO: 修改为软件提供给您的APP_ID与APP_SECREAT

define('APP_ID', '软件提供的APP_ID');
define('APP_SECREAT', '软件提供的APP_SECREAT');

// TODO: 修改为您的数据库资料
define('DB_HOST', 'localhost');
define('DB_PORT', '');
define('DB_USER', '您的数据库用户名');
define('DB_PASS', '您的数据库密码');
define('DB_DBASE', '您的数据库名称');

require_once 'restler/restler.php';
require_once 'error.php';

spl_autoload_register('spl_autoload');
$r = new Restler();
$r->addAPIClass('Book');
$r->addAPIClass('User');
//此方法用于身份验证
$r->addAuthenticationClass('BaseAuth');
$r->handle();
